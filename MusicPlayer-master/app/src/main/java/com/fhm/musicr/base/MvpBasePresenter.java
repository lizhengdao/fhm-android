package com.fhm.musicr.base;

import com.fhm.musicr.datasource.models.ApiError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;

import io.reactivex.CompletableTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import retrofit2.adapter.rxjava2.HttpException;

public abstract class MvpBasePresenter<V extends MvpView> {
    private V mView;
    private CompositeDisposable compositeDisposable;

    public void onAttachView(V mvpView) {
        this.mView = mvpView;
        compositeDisposable = new CompositeDisposable();
    }

    public void addSubscribe(Disposable disposable) {
        if (compositeDisposable != null) {
            compositeDisposable.add(disposable);
        }
    }

    public void onDetachView() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        this.mView = null;
    }

    public V getView() {
        return mView;
    }

    protected void handleResponseError(Throwable e) {
        if (e instanceof SocketTimeoutException) {
            getView().showTimeoutConnection();
        }  else {
            getView().showError(parseError(e).getErrorMessage());
        }
    }

    protected ApiError parseError(Throwable e) {
        ApiError apiError = new ApiError();
        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            Response<?> response = httpException.response();
            try {
                String strErrorBody = response.errorBody().string();
                if (!(strErrorBody.equals(""))) {
                    JSONObject bodyObj = new JSONObject(strErrorBody);
                    apiError.setStatus(bodyObj.optInt("status"));
                    apiError.setType(bodyObj.optString("type"));
                    if (bodyObj.has("error")) {
                        JSONObject errorObj = bodyObj.getJSONObject("error");
                        apiError.setErrorCode(errorObj.getString("code"));
                        apiError.setErrorMessage(errorObj.getString("message"));
                    }
                } else {
                    apiError.setStatus(response.code());
                    apiError.setErrorMessage(response.code() + " - " + response.message());
                }
            } catch (JSONException | IOException ex) {
                apiError.setStatus(response.code());
                apiError.setErrorMessage(ex.getMessage());
            }
        } else {
            apiError.setErrorMessage(e.getMessage());
        }
        return apiError;
    }

    protected <T> SingleTransformer<T, T> applySingleSchedule() {
        return upstream -> upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    protected <T> CompletableTransformer applyCompletableSchedule() {
        return upstream -> upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    protected <T> ObservableTransformer<T, T> applyObservableSchedule() {
        return upstream -> upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
