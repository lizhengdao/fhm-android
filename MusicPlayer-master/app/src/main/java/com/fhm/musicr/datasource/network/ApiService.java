package com.fhm.musicr.datasource.network;

import com.fhm.musicr.datasource.models.BaseResponse;
import com.fhm.musicr.datasource.models.ForgetPassRequest;
import com.fhm.musicr.datasource.models.LoginRequest;
import com.fhm.musicr.datasource.models.LoginResponse;
import com.fhm.musicr.datasource.models.UserProfileRequest;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

public interface ApiService {
//Liệt kê API
//    @POST("/auth/login")
//    Single<BaseResponse<LoginResponse>> login(@Body LoginRequest body);
    @POST("/api/me")
    Single<BaseResponse<LoginResponse>> login(@Body LoginRequest body);

    @DELETE("/api/me")
    Single<BaseResponse> logout(@Header("Authorization") String token);

    @POST("/api/signin")
    Single<BaseResponse> signUp(@Body UserProfileRequest body);

    @POST("/api/forgot_pass")
    Single<BaseResponse> forgotPass(@Body ForgetPassRequest body);

    @PUT("/api/me")
    Single<BaseResponse> updateProfile(@Header("Authorization") String token, @Body UserProfileRequest body);

    @GET("/api/get_songs")
    Single<BaseResponse> getSongs(@Header("Authorization") String token);

    @Multipart
    @POST("/api/image/upload")
    Single<BaseResponse> uploadImage(@Header("Authorization") String token, @Part MultipartBody.Part body);
}
