package com.fhm.musicr.ui.page.forgetpass;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fhm.musicr.App;
import com.fhm.musicr.R;
import com.fhm.musicr.base.MvpBaseFragment;
import com.fhm.musicr.datasource.models.ForgetPassRequest;
import com.fhm.musicr.ui.widget.rangeseekbar.OnRangeChangedListener;
import com.fhm.musicr.ui.widget.rangeseekbar.RangeSeekBar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ForgetPassFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ForgetPassFragment extends MvpBaseFragment<ForgetPassPresenter> implements OnRangeChangedListener, ForgetPassContract.View  {

    @BindView(R.id.btn_forget_pass)
    Button btnForgetPass;

    @BindView(R.id.editTxtEmailAddress_forgetPass)
    EditText txtEmailAddress;
    public ForgetPassFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ForgetPassFragment newInstance() {
        ForgetPassFragment fragment = new ForgetPassFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent().inject(this);
    }

    @Nullable
    @Override
    protected View onCreateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.forget_pass, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        onClick();
    }
    private void onClick() {
        btnForgetPass.setOnClickListener(v -> {
            getPresenter().forgotPass(new ForgetPassRequest("normal", txtEmailAddress.getText().toString().trim()));
        });
    }

    @Override
    public void navigateMainScreen() {
//        super.onBackPressed();
    }


    @Override
    public void showError(String message) {

    }

    @Override
    public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {

    }

    @Override
    public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

    }

    @Override
    public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

    }
}