package com.fhm.musicr.ui.page.profilepage;

import android.util.Log;

import com.fhm.musicr.App;
import com.fhm.musicr.base.MvpBasePresenter;
import com.fhm.musicr.datasource.models.UserProfileRequest;
import com.fhm.musicr.datasource.repository.AppRepository;

import java.net.SocketTimeoutException;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;

public class ProfilePresenter extends MvpBasePresenter<ProfileContract.View> implements  ProfileContract.Presenter {

    private AppRepository mAppRepository;

    @Inject
    public ProfilePresenter(AppRepository appRepository) {
        this.mAppRepository = appRepository;
    }

    //Cấu hình API
    @Override
    public void logout() {
        System.out.println("Logout Presenter");
        getView().showLoading();
        addSubscribe(mAppRepository.logout("Bearer "+App.getInstance().getPreferencesUtility().getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    getView().hideLoading();
                    App.getInstance().getPreferencesUtility().setToken("");
                    Log.d("MIEN-SUCCESS", r.getData().toString());
                }, e -> {
                    Log.d("MIEN-FAILS", e.getMessage());
                    getView().hideLoading();
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));
    }

    @Override
    public void updateProfile(UserProfileRequest userProfileRequest) {
        System.out.println("forgotPass Presenter");
        getView().showLoading();
        addSubscribe(mAppRepository.updateProfile("Bearer "+App.getInstance().getPreferencesUtility().getToken(), userProfileRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    getView().hideLoading();
                    Log.d("MIEN-SUCCESS", r.getData().toString());
                }, e -> {
                    Log.d("MIEN-FAILS", e.getMessage());
                    getView().hideLoading();
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));
    }

    @Override
    public void uploadImage(MultipartBody.Part body) {
        getView().showLoading();
        Log.d("TEST", body.toString());
        addSubscribe(mAppRepository.uploadImage("Bearer "+App.getInstance().getPreferencesUtility().getToken(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    getView().hideLoading();
                    Log.d("MIEN-SUCCESS", r.getData().toString());
                }, e -> {
                    Log.d("MIEN-FAILS", e.getMessage());
                    getView().hideLoading();
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));
    }

    @Override
    public void getSongs() {
        Log.d("HUAN-SUCCESS", "Get songs");
        addSubscribe(mAppRepository.getSongs("Bearer "+App.getInstance().getPreferencesUtility().getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    Log.d("", r.getData().toString());
                    getView().navigateMainScreen(r.getData());
                }, e -> {
                    Log.d("HUAN-FAILS", e.getMessage());
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));
    }
}
