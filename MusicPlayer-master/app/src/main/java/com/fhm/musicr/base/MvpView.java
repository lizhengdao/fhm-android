package com.fhm.musicr.base;

import androidx.annotation.StringRes;

public interface MvpView {
    void showLoading();

    void hideLoading();

    void showError(String message);

    void showError(@StringRes int resString);

    void showTimeoutConnection();

    void showNoInternetConnection();

}
