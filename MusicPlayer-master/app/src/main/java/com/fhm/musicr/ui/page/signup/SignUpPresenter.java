package com.fhm.musicr.ui.page.signup;

import android.util.Log;

import com.fhm.musicr.base.MvpBasePresenter;
import com.fhm.musicr.datasource.models.UserProfileRequest;
import com.fhm.musicr.datasource.repository.AppRepository;

import java.net.SocketTimeoutException;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SignUpPresenter extends MvpBasePresenter<SignUpContract.View> implements  SignUpContract.Presenter {

    private AppRepository mAppRepository;

    @Inject
    public SignUpPresenter(AppRepository appRepository) {
        this.mAppRepository = appRepository;
    }

    //Cấu hình API
    @Override
    public void signUp(UserProfileRequest userProfileRequest) {
        System.out.println("forgotPass Presenter");
        getView().showLoading();
        addSubscribe(mAppRepository.signUp(userProfileRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    getView().hideLoading();
                    Log.d("MIEN-SUCCESS", r.getData().toString());
                }, e -> {
                    Log.d("MIEN-FAILS", e.getMessage());
                    getView().hideLoading();
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));
    }
}
