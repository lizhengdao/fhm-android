package com.fhm.musicr.ui.page.accountpage;

import com.fhm.musicr.base.MvpView;
import com.fhm.musicr.datasource.models.ForgetPassRequest;
import com.fhm.musicr.datasource.models.LoginRequest;
import com.fhm.musicr.datasource.models.UploadImageRequest;
import com.fhm.musicr.datasource.models.UserProfileRequest;

public interface AccountContract {
    //khai báo api
    interface Presenter {
        void login(LoginRequest loginRequest);
        void logout();
        void forgotPass(ForgetPassRequest forgetPassRequest);
        void signUp(UserProfileRequest userProfileRequest);
        void updateProfile(UserProfileRequest userProfileRequest);
        void uploadImage(UploadImageRequest uploadImageRequest);
    }

//trả về màn hình sau khi gọi API thành công
    interface View extends MvpView {
        void navigateMainScreen();
    }

}
