package com.fhm.musicr;

import android.app.Application;

import com.fhm.musicr.di.AppComponent;
import com.fhm.musicr.di.AppModule;
import com.fhm.musicr.di.DaggerAppComponent;
import com.fhm.musicr.di.NetworkModule;
import com.fhm.musicr.util.PreferenceUtil;



public class App extends Application {
    private static App mInstance;
    private AppComponent appComponent;
    public static synchronized App getInstance() {
        return mInstance;
    }

    public PreferenceUtil getPreferencesUtility() {
        return PreferenceUtil.getInstance(App.this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();

    //    Nammu.init(this);

    }

    public static AppComponent getAppComponent() {
        return mInstance.appComponent;
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}