package com.fhm.musicr.datasource.repository;

import com.fhm.musicr.datasource.models.BaseResponse;
import com.fhm.musicr.datasource.models.ForgetPassRequest;
import com.fhm.musicr.datasource.models.LoginRequest;
import com.fhm.musicr.datasource.models.LoginResponse;
import com.fhm.musicr.datasource.models.UserProfileRequest;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import retrofit2.http.Part;

public interface AppRepository {
    // Liệt kê API
    Single<BaseResponse<LoginResponse>> login(LoginRequest body);
    Single<BaseResponse> signUp(UserProfileRequest body);
    Single<BaseResponse> forgotPass(ForgetPassRequest body);
    Single<BaseResponse> logout(String token);
    Single<BaseResponse> updateProfile(String token, UserProfileRequest body);
    Single<BaseResponse> uploadImage(String token, @Part MultipartBody.Part body);
    Single<BaseResponse> getSongs(String token);
}
