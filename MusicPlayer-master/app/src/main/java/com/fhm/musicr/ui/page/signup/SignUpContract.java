package com.fhm.musicr.ui.page.signup;

import com.fhm.musicr.base.MvpView;
import com.fhm.musicr.datasource.models.UserProfileRequest;

public interface SignUpContract {
    //khai báo api
    interface Presenter {
        void signUp(UserProfileRequest userProfileRequest);
    }

//trả về màn hình sau khi gọi API thành công
    interface View extends MvpView {
        void navigateMainScreen();
    }

}
