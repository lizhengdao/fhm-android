package com.fhm.musicr.ui.page.forgetpass;

import com.fhm.musicr.base.MvpView;
import com.fhm.musicr.datasource.models.ForgetPassRequest;

public interface ForgetPassContract {
    //khai báo api
    interface Presenter {
        void forgotPass(ForgetPassRequest forgetPassRequest);
    }

//trả về màn hình sau khi gọi API thành công
    interface View extends MvpView {
        void navigateMainScreen();
    }

}
