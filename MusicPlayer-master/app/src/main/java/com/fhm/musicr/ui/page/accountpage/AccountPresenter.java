package com.fhm.musicr.ui.page.accountpage;

import android.util.Log;

import com.fhm.musicr.App;
import com.fhm.musicr.base.MvpBasePresenter;
import com.fhm.musicr.datasource.models.ForgetPassRequest;
import com.fhm.musicr.datasource.models.LoginRequest;
import com.fhm.musicr.datasource.models.UploadImageRequest;
import com.fhm.musicr.datasource.models.UserProfileRequest;
import com.fhm.musicr.datasource.repository.AppRepository;

import java.net.SocketTimeoutException;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AccountPresenter extends MvpBasePresenter<AccountContract.View> implements  AccountContract.Presenter {

    private AppRepository mAppRepository;

    @Inject
    public AccountPresenter(AppRepository appRepository) {
        this.mAppRepository = appRepository;
    }

    //Cấu hình API
    @Override
    public void login(LoginRequest loginRequest) {
        getView().showLoading();
        addSubscribe(mAppRepository.login(loginRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    getView().hideLoading();
                    App.getInstance().getPreferencesUtility().setToken(r.getData().getToken());
                    App.getInstance().getPreferencesUtility().setUserEmail(r.getData().getProfile().getEmail());
                    App.getInstance().getPreferencesUtility().setUserName(r.getData().getProfile().getName());
                    App.getInstance().getPreferencesUtility().setUserAvatar(r.getData().getProfile().getAvatar());
                    App.getInstance().getPreferencesUtility().setUserId(r.getData().getProfile().getId().toString());

                    Log.d("TEST", "Ava "+ App.getInstance().getPreferencesUtility().getUserAvatar());
                    Log.d("TEST", "Token "+ App.getInstance().getPreferencesUtility().getToken());
                    Log.d("TEST", "Email "+ App.getInstance().getPreferencesUtility().getUserEmail());
                    Log.d("TEST", "Name "+ App.getInstance().getPreferencesUtility().getUserName());
                    getView().navigateMainScreen();
                    Log.d("MIEN-SUCCESS", r.getData().toString());
//                    if (r.getData().getIsFisrtLogin()) {
//                        getView().navigateChangePassword();
//                    } else {
//                        getView().navigateMainScreen();
//                    }
                }, e -> {
                    Log.d("MIEN-FAILS", e.getMessage());
                    getView().hideLoading();
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));

    }

    @Override
    public void logout() {
        System.out.println("Logout Presenter");
        getView().showLoading();
        addSubscribe(mAppRepository.logout(App.getInstance().getPreferencesUtility().getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    getView().hideLoading();
                    App.getInstance().getPreferencesUtility().setToken("");
                    Log.d("MIEN-SUCCESS", r.getData().toString());
//                    if (r.getData().getIsFisrtLogin()) {
//                        getView().navigateChangePassword();
//                    } else {
//                        getView().navigateMainScreen();
//                    }
                }, e -> {
                    Log.d("MIEN-FAILS", e.getMessage());
                    getView().hideLoading();
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));
    }

    @Override
    public void forgotPass(ForgetPassRequest forgetPassRequest) {
        System.out.println("forgotPass Presenter");
        getView().showLoading();
        addSubscribe(mAppRepository.forgotPass(forgetPassRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    getView().hideLoading();
                    Log.d("MIEN-SUCCESS", r.getData().toString());
                }, e -> {
                    Log.d("MIEN-FAILS", e.getMessage());
                    getView().hideLoading();
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));
    }

    @Override
    public void signUp(UserProfileRequest userProfileRequest) {
        System.out.println("forgotPass Presenter");
        getView().showLoading();
        addSubscribe(mAppRepository.signUp(userProfileRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    getView().hideLoading();
                    Log.d("MIEN-SUCCESS", r.getData().toString());
                }, e -> {
                    Log.d("MIEN-FAILS", e.getMessage());
                    getView().hideLoading();
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));
    }

    @Override
    public void updateProfile(UserProfileRequest userProfileRequest) {
        System.out.println("forgotPass Presenter");
        getView().showLoading();
        addSubscribe(mAppRepository.updateProfile(App.getInstance().getPreferencesUtility().getToken(), userProfileRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    getView().hideLoading();
                    Log.d("MIEN-SUCCESS", r.getData().toString());
                }, e -> {
                    Log.d("MIEN-FAILS", e.getMessage());
                    getView().hideLoading();
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));
    }

    @Override
    public void uploadImage(UploadImageRequest uploadImageRequest) {

    }
}
