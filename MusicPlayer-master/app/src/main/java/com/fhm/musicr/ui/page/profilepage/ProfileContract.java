package com.fhm.musicr.ui.page.profilepage;

import com.fhm.musicr.base.MvpView;
import com.fhm.musicr.datasource.models.UserProfileRequest;

import okhttp3.MultipartBody;

public interface ProfileContract {
    //khai báo api
    interface Presenter {
        void logout();
        void updateProfile(UserProfileRequest userProfileRequest);
        void uploadImage(MultipartBody.Part body);
        void getSongs();
    }

//trả về màn hình sau khi gọi API thành công
    interface View extends MvpView {
        void navigateMainScreen( Object data );
    }

}
