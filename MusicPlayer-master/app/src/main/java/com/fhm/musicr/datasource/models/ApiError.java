package com.fhm.musicr.datasource.models;

public class ApiError {
    private int status;
    private String type;
    private String errorMessage;
    private String errorCode;

    public int getStatus() {
        return status;
    }

    public ApiError setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getType() {
        return type;
    }

    public ApiError setType(String type) {
        this.type = type;
        return this;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public ApiError setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
